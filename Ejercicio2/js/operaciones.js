'use strict'

function operacionesFactorial(){
	var entrada = parseInt(document.getElementById("entradaFactorial").value);
	var salidaR = document.getElementById("salidaFactorialRecursivo");
	var salida = document.getElementById("salidaFactorial");
	var resultado = factorialRecursivo(entrada);
	salidaR.innerHTML = "Recursivo: "+resultado;
	resultado = factorial(entrada);
	salida.innerHTML = "Normal: "+resultado;
}

function factorialRecursivo(numero){
	var result = 0;
	if (numero == 1) {
		return 1;
	}else{
		result = numero * factorialRecursivo(numero-1);
	}
	return result;
}

function factorial(numero){
	var result = 1;
	for (var i = numero; i >= 1; i--) {
		result = result * i;
	}
	return result;
}

function validarEntrada(){
	var entrada = document.getElementById("entradaFactorial");
	var entradaAux = parseInt(entrada.value);
	if(entradaAux<2 || entradaAux>20){
		entrada.value = 1;
	}
}